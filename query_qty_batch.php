<?php
include "callAPI.php";
ini_set('max_execution_time',5000); //3000 seconds = 50 minutes
echo "DATE:".date("dmYhis");

$endpoint= 'http://api.reebonz.com/gate';
$product_list_url = '/api/wasabi/ois_product_list';
$product_qty_url = '/api/wasabi/ois_product_qty';
//echo 'Call to API >>'.$_POST['productcode'];

$csv_mimetypes = array('text/csv', 'text/plain', 'application/csv', 'text/comma-separated-values', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'text/anytext', 'application/octet-stream', 'application/txt');
if (in_array($_FILES['file']['type'], $csv_mimetypes)) 
{
	/* Grab the location of this PHP script and change the path to a different location where we can save the data */
	$filePathRaw = dirname(__FILE__);
	$filePathSegments = explode("/", $filePathRaw);
	$filePath="";
	for ($x = 1; $x < (sizeof($filePathSegments) - 3); $x++) 
	{
		$filePath = $filePath . "/" . $filePathSegments[$x];
	}

	/* Generate a filename for the CSV file */
	$token = date("YmdHis");
	/* Save the CSV data */
	$rawCSV = file_get_contents($_FILES['file']['tmp_name']);
	$lines=explode("\n",$rawCSV);
	echo "<table border=1>";
	echo '<td>Code</td><td>Sub-code</td><td>Reebonz Qty</td><td>Yahoo Qty</td>';
	$csv='code,sub-code,Reebonz quantity,Yahoo quantity'.PHP_EOL;
	
	foreach ($lines as $i=> $line)
	{

		$sub_code = '' ;
		$yahoo_qty='';
	 	$values = explode(',', $line); // split lines by commas
		$sku ="";$sku = str_replace('"','',$values[0]);
		$sub_code =''; $sub_code = str_replace('"','',$values[1]) ;
		$yahoo_qty ="";$yahoo_qty = str_replace('"','',$values[2]);

		if (isset($sub_code))
		{
			$response =CallAPI("GET",$endpoint.$product_qty_url."?sku=".$sku);

			$json= json_decode($response,true);
			$doc ="";$doc = $json['response']['docs'];

			if (isset($doc))
			{		
				foreach ($doc as $valueQty)
				{
					if ($valueQty['item_no']=$sub_code)
					{	
						if ($yahoo_qty !=$valueQty['qty'] ){
							echo "<tr bgcolor='red'>";
						}
						else
						{
							echo "<tr>";
						}
						
						echo  '<td>'.$valueQty['sku'].'</td><td>'.$valueQty['item_no'].'</td>';
						echo '<td>'.$valueQty['qty'].'</td>';
						echo '<td>'.$yahoo_qty . '</td>';

						echo '</tr>';
						$csv.= $valueQty['sku'].','.$valueQty['item_no'].','.$valueQty['qty'].',';
						$csv.= $yahoo_qty;
						$csv.=PHP_EOL;
						break;
					}

					
					
					// $cnt +=1;
				}
			}
		}
	}
	echo '</table>';
	messagebox('Completed');
}

$DownloadPath="./cache";
	$DownloadQuantityCSV = $DownloadPath."/check_qty_list_".date("dmYhis").".csv";

//Product csv
	// $myfile = fopen($DownloadPath."/productlist_".date("dmYhis").".csv", "w") or die("Unable to open file!");
	// fwrite($myfile, mb_convert_encoding($csv_header.$csv, 'Shift_JIS'));
	// fclose($myfile);

$myfile = fopen($DownloadPath."/check_qty_list_".date("dmYhis").".csv", "w") or die("Unable to open file!");
//$myfile = fopen("./cache/check_qty_list_".date("dmYhis").".csv", "w") or die("Unable to open file!");
//fwrite($myfile, $csv);
fwrite($myfile, mb_convert_encoding($csv, 'Shift_JIS'));
fclose($myfile);
	echo '<br><a href="'.$DownloadQuantityCSV.'">Download Quantity CSV</a>';
// echo '<br><a href="./cache/check_qty_list_".date("dmYhis").".csv">Download CSV</a>';
echo '<br><a href="product_qty_list.htm">Return to previous</a>';

?>