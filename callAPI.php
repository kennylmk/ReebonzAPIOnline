<?php
ini_set('max_execution_time',5000); //3000 seconds = 50 minutes

    function CallGoogleTranslateAPI($source, $target, $text)
    {
        //$source = en
        //$target=ja
        // Request translation
        $response = requestTranslation($source, $target, $text);
        // Get translation text
        // $response = self::getStringBetween("onmouseout=\"this.style.backgroundColor='#fff'\">", "</span></div>", strval($response));
        // Clean translation
        $translation = getSentencesFromJSON($response);
        return $translation;
    }
    /**
     * Internal function to make the request to the translator service
     *
     * @internal
     *
     * @param string $source
     *            Original language taken from the 'translate' function
     * @param string $target
     *            Target language taken from the ' translate' function
     * @param string $text
     *            Text to translate taken from the 'translate' function
     *
     * @return object[] The response of the translation service in JSON format
     */
    function requestTranslation($source, $target, $text)
    {
        // Google translate URL
        $url = "https://translate.google.com/translate_a/single?client=at&dt=t&dt=ld&dt=qca&dt=rm&dt=bd&dj=1&hl=es-ES&ie=UTF-8&oe=UTF-8&inputm=2&otf=2&iid=1dd3b944-fa62-4b55-b330-74909a99969e";
        $fields = array(
            'sl' => urlencode($source),
            'tl' => urlencode($target),
            'q' => urlencode($text)
        );
        if(strlen($fields['q'])>=5000)
            throw new \Exception("Maximum number of characters exceeded: 5000");
        
        // URL-ify the data for the POST
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1');
        // Execute post
        $result = curl_exec($ch);
        // Close connection
        curl_close($ch);
        return $result;
    }
    /**
     * Dump of the JSON's response in an array
     *
     * @param string $json
     *            The JSON object returned by the request function
     *
     * @return string A single string with the translation
     */
    function getSentencesFromJSON($json)
    {
        $sentencesArray = json_decode($json, true);
        $sentences = "";
        foreach ($sentencesArray["sentences"] as $s) {
            $sentences .= isset($s["trans"]) ? $s["trans"] : '';
        }
        return $sentences;
    }

function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;

        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;

        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //curl_setopt($curl, CURLOPT_USERPWD, "username:password");
  

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);
    // $status = curl_getinfo($result, CURLINFO_HTTP_CODE);
    // if ($status == 200)
    // {
    //     echo "Successfully Sended";
    // }
    // else
    // {
    //     echo "Try Again";
    // }
  //echo '<br>'.$result.'<br><br>';
    curl_close($curl);

    return $result;
}

function CallReebonzAPI($method, $url, $token,$data=false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // curl_setopt($curl, CURLOPT_USERPWD, "username:password");

	$authorization = "'Authorization: Bearer ".$token."'";
	curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json' , $authorization ));
    echo '<br>'. $authorization ;
    // curl_setopt($curl,CURLOPT_HTTPHEADER,array('Accept: application/json' , $authorization ));
    curl_setopt($curl, CURLOPT_URL, $url);
    echo '<br>'. $url;
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

    $result = curl_exec($curl);
    $error = curl_errno($curl);
     $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

echo '<br>'. $error;
     echo '<br>'.$status;
          echo '<br>'.$result;

    curl_close($curl);
  // echo '<br>'.$status;

    return $result;
}

function CallNEAPI($method, $url, $data)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        // default:
            // if ($data)
                // $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // curl_setopt($curl, CURLOPT_USERPWD, "username:password");

	// $authorization = "Authorization: Bearer ".$token;
	// curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json' , $authorization ));
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);
    // echo $result;
    curl_close($curl);

    return $result;
}
/* //echo "Calling API";
$csv='sku,brand,images url'.PHP_EOL;
for ($x=0; $x <=60; $x++)
{
	echo 'http://api.reebonz.com/gate/api/wasabi/ois_product_list?updated_at_after=2017-01-01T00:00:00Z&start='.$x.'&rows=500<br>'.PHP_EOL;
$response = CallAPI("GET","http://api.reebonz.com/gate/api/wasabi/ois_product_list?updated_at_after=2017-01-01T00:00:00Z&start=".$x."&rows=500");
//echo "Finished";
//echo $response;
//$response=json_decode($response);

$json = json_decode($response,true);
//echo "Start reading";
//foreach ($json as $jsondata => $json_field) {
//    echo $person_a['response'];
//echo $json->response->numFound;
$doc = $json['response']['docs'];
//echo $doc;

foreach ($doc as $value){
	//echo $value['sku'].',';

	$csv.=$value['sku'].',';
	

	//echo $value['brand'].',';
	$csv .=$value['brand'].',';
	$imagelist = $value['images'];
	foreach($imagelist as $img){
		//echo $img.',';
		$csv .=$img.',';
	}
	$csv .=PHP_EOL;
	//echo '\n';
	//$csv=str_getcsv($csv,"\r\n ");
} 
}
echo $csv;
$myfile = fopen("C:/share/json.csv", "w") or die("Unable to open file!");
fwrite($myfile, $csv);
fclose($myfile);
echo '<br>End';
	//echo $doc;
//foreach($json as $value){
	//$sku = $value->sku;
	//echo $sku;
	//echo $value->sku;
//} */

function messagebox($msg)
{
		echo '<script type="text/javascript"> alert("'.$msg.'")</script>';
}
?>