<?php
include "callAPI.php";
ini_set('max_execution_time',300); //3000 seconds = 50 minutes

//echo 'Call to API >>'.$_POST['productcode'];

$endpoint= 'http://api.reebonz.com/gate';
$product_info_url = '/api/wasabi/ois_product_info';
$product_qty_url = '/api/wasabi/ois_product_qty';
$parameter = '?sku='.$_POST['productcode'];

echo 'URL = '.$endpoint.$product_info_url.$parameter.'<p>';
$response = CallAPI("GET",$endpoint.$product_info_url.$parameter);
$json = json_decode($response,true);
$doc = $json['response']['docs'];

//var_dump($json);

echo '<table border=1>';
foreach ($doc as $value){
	echo '<tr><td>Sku:</td><td>'.$value['sku'].'</td></tr>';
	echo '<tr><td>Brand:</td><td>'.$value['brand'].'</td></tr>';
	echo '<tr><td>Parent Category:</td><td>'.$value['parent_category'].'</td></tr>';
	echo '<tr><td>Sub Category:</td><td>'.$value['sub_category'].'</td></tr>';
	echo '<tr><td>Updated AT:</td><td>'.$value['updated_at'].'</td></tr>';
	echo '<tr><td>Title:</td><td>'.$value['title_en'].'</td></tr>';
	echo '<tr><td>Description:</td><td>'.$value['description_en'].'</td></tr>';
	echo '<tr><td>Currency(JPY):</td><td>'.$value['currency_jp'].'</td></tr>';
	echo '<tr><td>Retail Price (JPY):</td><td>'.$value['retail_price_jp'].'</td></tr>';
	echo '<tr><td>Selling Price (JPY):</td><td>'.$value['selling_price_jp'].'</td></tr>';
	
	echo '<tr><td>Color:</td><td>';
	//echo $value['color_en'][0];
	if (isset($value['color_en'])){
		foreach ($value['color_en'] as $color){
				echo $color;
		}
	}
 	echo '</td></tr>';
	foreach ($value['color_codes'] as $color_codes) 
	{
		
	}
	echo '<tr><td>Color Codes:</td><td>'.$value['color_codes'][0].'</td></tr>';
	echo '<tr><td>Gender:</td><td>'.$value['gender_en'][0].'</td></tr>';
	
	echo '<tr><td>weight_en:</td><td>'.$value['weight_en'].'</td></tr>';
	
	echo '<tr><td>measurement_en:</td><td>'.$value['measurement_en'].'</td></tr>';
	
	$material_en='';
	foreach ($value['material_en'] as $material)
	{
		$material_en .=$material ;
	}
	echo '<tr><td>material_en:</td><td>'.$material_en.'</td></tr>';

	echo '<tr><td>Images:</td><td>';     
	foreach ($value['images'] as $image){
		echo '<a href='.$image.' target=_blank>'.$image.'<a><br>'; 
	}
	echo '</td></tr>';
}

echo '</table>';

//echo 'URL >> '.$endpoint.$product_qty_url.$parameter.'<p>';
$responseQty = CallAPI("GET",$endpoint.$product_qty_url.$parameter);

$jsonQty = json_decode($responseQty,true);
$docQty = $jsonQty['response']['docs'];

echo '<table border=1>';
	echo '<tr>';
	echo '<td>Item Id.</td>';
 	echo '<td>Item No.</td>';
	echo '<td>Size Code</td>';
	echo '<td>Size Label</td>';
	echo '<td>Qty</td>';
	echo '<td>Updated</td>'; 
	echo '</tr>';
foreach ($docQty as $valueQty){
	echo '<tr>';
	echo '<td>'.$valueQty['item_id'].'</td>';
 	echo '<td>'.$valueQty['item_no'].'</td>';
	echo '<td>'.$valueQty['size_code'].'</td>';
	echo '<td>'.$valueQty['size_label'].'</td>';
	echo '<td>'.$valueQty['qty'].'</td>';
	echo '<td>'.$valueQty['updated_at'].'</td>'; 
	echo '</tr>';
}
echo '</table>';
echo '<a href="query_product.htm">Return to previous</a>'
?>