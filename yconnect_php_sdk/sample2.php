<?php
require("lib/YConnect.inc");
// アプリケーションID, シークレッvト
$client_id     = "dj0zaiZpPWNhTTlQRlJYZ1k1dCZzPWNvbnN1bWVyc2VjcmV0Jng9NDE-";
$client_secret = "09c6b9ea3e1ce954642d159ffbf8a2e348c51076";
// echo $_SERVER["SERVER_NAME"] ; exit();
// 各パラメータ初期化
//$redirect_uri = "http://" . $_SERVER["SERVER_NAME"] . $_SERVER["PHP_SELF"];
$redirect_uri="https://www.reebonz.com/jp";
// リクエストとコールバック間の検証用のランダムな文字列を指定してください
$state = "44Oq44Ki5YWF44Gr5L+644Gv44Gq44KL77yB";
// リプレイアタック対策のランダムな文字列を指定してください
$nonce = "5YOV44Go5aWR57SE44GX44GmSUTljqjjgavjgarjgaPjgabjgog=";

$response_type = OAuth2ResponseType::CODE_IDTOKEN;
$scope = array(
    OIDConnectScope::OPENID,
    OIDConnectScope::PROFILE,
    OIDConnectScope::EMAIL,
    OIDConnectScope::ADDRESS
);
$display = OIDConnectDisplay::DEFAULT_DISPLAY;
$prompt = array(
    OIDConnectPrompt::DEFAULT_PROMPT
);

// クレデンシャルインスタンス生成
$cred = new ClientCredential( $client_id, $client_secret );
 //echo "cred>>>>".$cred;//exit(); 
$client = new YConnectClient( $cred );
 //echo var_dump($client) ;exit(); 
// 認可コードを取得...(1)
$code_result = $client->getAuthorizationCode( $state );
 //echo var_dump($code_result);//exit();
if( $code_result ) {
 // echo ">>>>".$code_result;exit();
    try {
        // Tokenエンドポイントにリクエスト...(2)
        $client->requestAccessToken(
            $redirect_uri,
            $code_result
        );
    } catch ( OAuth2TokenException $e ) {
        if( $e->invalidGrant() ) {
            // 再度Authorizationエンドポイントへリクエスト...(3)
        }
    }
 
    try {
        // IDトークンを検証...(4)
        $client->verifyIdToken( $nonce );
        // アクセストークン、リフレッシュトークンを取得...(5)
        $accessToken  = $client->getAccessToken();
        $refreshToken = $client->getRefreshToken();
    } catch ( IdTokenException $e ) {
        // 認証失敗...(6)
        echo "Unauthorized";
    }
    // ユーザー識別子を取得...(7)
    $userId = $client->getIdToken();
}
else
{
       /****************************
             Access Token Request    
        ****************************/
        // Tokenエンドポイントにリクエスト
                             // echo "ACCESS TOKEN";exit();
        $client->requestAccessToken(
            $redirect_uri,
            $code_result
        );

        echo "<h1>Access Token Request</h1>";
        // アクセストークン, リフレッシュトークン, IDトークンを取得

        echo "ACCESS TOKEN : " . $client->getAccessToken() . "<br/><br/>";

        echo "REFRESH TOKEN: " . $client->getRefreshToken() . "<br/><br/>";
        echo "EXPIRATION   : " . $client->getAccessTokenExpiration() . "<br/><br/>";

        /*****************************
             Verification ID Token
        *****************************/

        // IDトークンを検証
        $client->verifyIdToken( $nonce );
        echo "ID TOKEN: <br/>";
        echo "<pre>" . print_r( $client->getIdToken(), true ) . "</pre>";

        /************************
             UserInfo Request
        ************************/

        // UserInfoエンドポイントにリクエスト
        $client->requestUserInfo( $client->getAccessToken() );
        echo "<h1>UserInfo Request</h1>";
        echo "UserInfo: <br/>";
        // UserInfo情報を取得
        echo "<pre>" . print_r( $client->getUserInfo(), true ) . "</pre>";
}
?>