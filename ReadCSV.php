<?php
if (isset($_POST['submit']) )
{

	$csv_mimetypes = array('text/csv', 'text/plain', 'application/csv', 'text/comma-separated-values', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'text/anytext', 'application/octet-stream', 'application/txt');
	if (in_array($_FILES['file']['type'], $csv_mimetypes)) 
	{
		/* Grab the location of this PHP script and change the path to a different location where we can save the data */
		$filePathRaw = dirname(__FILE__);
		$filePathSegments = explode("/", $filePathRaw);
		for ($x = 1; $x < (sizeof($filePathSegments) - 3); $x++) 
		{
			$filePath = $filePath . "/" . $filePathSegments[$x];
		}

		/* Generate a filename for the CSV file */
		$token = date("YmdHis");
		/* Save the CSV data */
		$rawCSV = file_get_contents($_FILES['file']['tmp_name']);
		$lines=explode("\n",$rawCSV);
		if (isset($lines))
		{
			echo "<html><body><table border=1>";
			foreach ($lines as $l => $line) 
			{
				$values=explode(',', $line);
				if (isset($values))
				{
					echo "<tr>";
					foreach ($values as $i => $item) 
					{
						echo "<td>".$item."</td>";
					}
					echo "</tr>";
				}

			}	
			echo "</table></body></html>";
		}

	}
}
else
{
	echo "<html>";
	echo "<body>";
	echo "<table>";
	echo "<form method='POST' action='ReadCSV.php' enctype='multipart/form-data'>";
	echo "<tr><td>CSV file:</td><td><input type=file name='file' title=Browse value='Browse'></td></tr>";
	echo "<tr><td></td><td><input type='submit' name='submit' value='Submit'></td></tr>";
	echo "</form>";
	echo "</table>";
	echo "</body>";
	echo "</html>";
}

?>